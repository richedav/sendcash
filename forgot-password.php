<?php

session_start();
$error=false;

//setting connection parameters
$user = "root";
$password = "Genes1s.";
$database_name = "sendCash";
$hostname = "192.168.0.188";
$dsn = 'mysql:dbname=' . $database_name . ';host=' . $hostname;

$UN=isset($_POST['UN']) ? urldecode($_POST['UN']) : NULL;

if (! is_null($UN)) {



    //initialize the connection to the database
    $conn = new PDO($dsn, $user, $password);

    $sth = $conn->prepare('
SELECT
email,password,id,name,mobile
FROM users
WHERE
email = :email and locked=0
LIMIT 1
');

    $sth->bindParam(':email', $UN);
    $sth->execute();
    $sql = $sth->fetch(PDO::FETCH_OBJ);


   if ($sth->rowCount()==1) {$error='ok';}
    else {$error='warn';}

}

?>

<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
    <meta charset="utf-8">
    <title>SendCash - Forgot Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/login.css" rel="stylesheet">
	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    
  </head>

  <body>

        <div id="wrapper">
            <div id="login">
                <form action="" class="form-login" method="post"> 
                    <div class="content-login">
                        <div class="header">
                            <a href="index.php" class="link-head-left"><i class="icon-home"></i></a>
                            <span>Forgot Password</span>
                            <a href="" class="link-head-right main-item"><i class="icon-config"></i></a>
                        </div>
                        
                        <div class="avatar"><img src="images/avatar.png" alt=""></div>
                        
                        <div class="inputs">
                            <?php
                            if ($error=='warn') {?>
                                <div class="error">Unable to locate user account</div>
                            <?php
                            }
                            ?>

                            <?php
                            if ($error=='ok') {?>
                                <div class="success">Password reset email sent</div>
                            <?php
                            }
                            ?>

                            <input name="UN" type="email" required placeholder="Email" />
                        </div>

                        <div class="clear"></div>
                        
                        <div class="button-login"><input type="submit" value="RESET PASSWORD"></div>
                    </div>
                    
                    <div class="footer-login">

                    </div>
                    
                   
                </form>

            </div>
     
       <div class="clear"></div>  
    <div class="link-to-page">Don't  have an account? <a href="sign-up.php">Sign up now!</a></div>
    </div>
    
  </body>
</html>

