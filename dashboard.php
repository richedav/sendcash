<?php

session_start();
include "include/security.php";
include_once "include/functions.php";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="David Riches">

        <title>iComply Global management console</title>

        <link href="css/style.default.css" rel="stylesheet">
        <link href="css/morris.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a href="dashboard.php" class="logo">
                        <img src="images/logo.png" alt="" />
                    </a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div><!-- header-left -->
                
                <div class="header-right">
                    
                    <div class="pull-right">
                        

                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="#"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-star"></i> Activity Log</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                              <li class="divider"></li>
                              <li><a href="logout.php"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
                            </ul>
                        </div><!-- btn-group -->
                        
                    </div><!-- pull-right -->
                    
                </div><!-- header-right -->
                
            </div><!-- headerwrapper -->
        </header>
        
        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">

                        <div class="media-body">
                            <h4 class="media-heading"><?php echo ucfirst($_SESSION['user']->name);?></h4>
                        </div>
                    </div><!-- media -->
                    
                    <h5 class="leftpanel-title">Navigation</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="dashboard.php"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li><a href="sendmoney.php"><span class="pull-right badge"></span><i class="fa fa-money"></i> <span>Send Money</span></a></li>
                        <li><a href="companies.php"><span class="pull-right badge"></span><i class="fa fa-mobile-phone"></i> <span>Send Airtime</span></a></li>
                        <li><a href="companies.php"><span class="pull-right badge"></span><i class="fa fa-credit-card"></i> <span>Get A Loan</span></a></li>
                        <li><a href="companies.php"><span class="pull-right badge"></span><i class="fa fa-globe"></i> <span>Get Insurance</span></a></li>
                        <li><a href="numbers.php"><span class="pull-right badge"></span><i class="glyphicon glyphicon-phone"></i> <span>Transaction History</span></a></li>
                    </ul>

                </div><!-- leftpanel -->
                
                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                <ul class="breadcrumb">
                                    <li><a href=""><?php "URL " . get_gravatar("david@candengo.com");  ?></a></li>
                                    <li>SendCash Portal</li>
                                </ul>
                                <h4>SendCash Portal</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->
                    
                    <div class="contentpanel">
                        
                        <div class="row row-stat">
                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                               </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-money"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">make payment</h5>
                                            <h1 class="mt5">Send Money</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Send Money</h5>
                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->






                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-mobile"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">make payment</h5>
                                            <h1 class="mt5">Send Airtime</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Send Airtime</h5>
                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->


                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-credit-card"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">coming soon</h5>
                                            <h1 class="mt5">Get Loan</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Send Airtime</h5>
                                            </div>
                                        </div>
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->




                            <div class="col-md-4">
                                <div class="panel panel-success-alt noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-globe"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">coming soon</h5>
                                            <h1 class="mt5">Get Insurance</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">
                                            <div class="pull-left">
                                                <h5 class="md-title nomargin">Send Airtime</h5>
                                            </div>

                                        </div>

                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->



                            
                            <div class="col-md-4">
                                <div class="panel panel-primary noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">
                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-clock-o"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">my account</h5>
                                            <h1 class="mt5">Transaction History</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">

                                        </div>

                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->





                            
                            <div class="col-md-4">
                                <div class="panel panel-dark noborder">
                                    <div class="panel-heading noborder">
                                        <div class="panel-btns">

                                        </div><!-- panel-btns -->
                                        <div class="panel-icon"><i class="fa fa-bullseye"></i></div>
                                        <div class="media-body">
                                            <h5 class="md-title nomargin">my profile</h5>
                                            <h1 class="mt5">Manage my Account</h1>
                                        </div><!-- media-body -->
                                        <hr>
                                        <div class="clearfix mt20">

                                            </div>
                                        </div>
                                        
                                    </div><!-- panel-body -->
                                </div><!-- panel -->
                            </div><!-- col-md-4 -->
                        </div><!-- row -->



                        
                    </div><!-- contentpanel -->
                    
                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>


        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/pace.min.js"></script>
        <script src="js/retina.min.js"></script>
        <script src="js/jquery.cookies.js"></script>
        
        <script src="js/flot/jquery.flot.min.js"></script>
        <script src="js/flot/jquery.flot.resize.min.js"></script>
        <script src="js/flot/jquery.flot.spline.min.js"></script>
        <script src="js/jquery.sparkline.min.js"></script>
        <script src="js/morris.min.js"></script>
        <script src="js/raphael-2.1.0.min.js"></script>
        <script src="js/bootstrap-wizard.min.js"></script>
        <script src="js/select2.min.js"></script>

        <script src="js/custom.js"></script>
        <script src="js/dashboard.js"></script>

    </body>
</html>
