<?php

session_start();
$error=false;

//setting connection parameters
$user = "root";
$password = "Genes1s.";
$database_name = "sendCash";
$hostname = "192.168.0.188";
$dsn = 'mysql:dbname=' . $database_name . ';host=' . $hostname;

$UN=isset($_POST['UN']) ? urldecode($_POST['UN']) : NULL;
$PW=isset($_POST['PW']) ? urldecode($_POST['PW']) : NULL;

if (! is_null($UN) and ! is_null($PW)) {

    //initialize the connection to the database
    $conn = new PDO($dsn, $user, $password);

    $sth = $conn->prepare('
SELECT
email,password,id,name,mobile
FROM users
WHERE
email = :email and locked=0
LIMIT 1
');

$sth->bindParam(':email', $UN);
$sth->execute();
$sql = $sth->fetch(PDO::FETCH_OBJ);


// Hashing the password with its hash as the salt returns the same hash

if ( crypt($PW, $sql->password) === $sql->password ) {

    // Get the user objest

    $user = new stdClass;
    $user->id = $sql->id;
    $user->name = $sql->name;
    $user->email = $sql->email;
    $user->mobile = $sql->mobile;

    $_SESSION['user']=$user;
    $error=true;

  //update the record to show last login and ip details

$sth = $conn->prepare('
update users
set lastlogin=:lastlogin,lastloginip=:ip,locked=0
where
email = :email
LIMIT 1
');

    $sth->bindParam(':email', $UN);
    $sth->bindParam(':lastlogin', date("Y-m-d H:i:s"));
    $sth->bindParam(':ip', $_SERVER['REMOTE_ADDR']);
    $sth->execute();
    $sql = $sth->fetch(PDO::FETCH_OBJ);

    header("Location: dashboard.php");
}
    else {$error=true;}

}

?>
<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
    <meta charset="utf-8">
    <title>Sendcash Payment Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Unimpossible">

    <!-- Le styles -->
    <link href="css/login.css" rel="stylesheet">

	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    
  </head>

  <body>

        <div id="wrapper">

                <form action="" class="form-login" method="post">


  
                        <div class="header">
                            <a href="index.php" class="link-head-left"><i class="icon-home"></i></a>
                            <span>Sign in</span>
                            <a href=""class="link-head-right main-item"><i class="icon-config"></i></a> 
                        </div>
                        
                        <div class="avatar"><img src="images/avatar.png" alt=""></div>



                        <div class="inputs">

                            <?php
                            if ($error==true) {?>
                                <div class="error">Unable to locate user account</div>
                            <?php
                            }
                            ?>

                            <input name="UN" type="text" required placeholder="Email Address" />
                            <input name="PW" type="password" required placeholder="Password" />
                
                        

                        <div class="link-2"><a href="forgot-password.php">Forgot Password?</a></div>
                        <div class="clear"></div>
                        
                        <div class="button-login"><input type="submit" value="Sign in"></div>
                    </div>

                    
                    <div class="footer-login">
                       <ul class="social-links">
                      	  <li class="twitter"><a href="#"><span>twitter</span></a></li>
                          <li class="google-plus"><a href="#"><span>google</span></a></li>
                          <li class="facebook"><a href="#"><span>facebook</span></a></li>
                       </ul>
                    </div>

                </form>


     
       <div class="clear"></div>  
    <div class="link-to-page">Don't  have an account? <a href="sign-up.php">Sign up now!</a></div>
    </div>
    
  </body>
</html>

