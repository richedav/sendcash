<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 06/09/14
 * Time: 20:38
 *
 * Checks for the user session and if not present present the login page;
 *
 */

session_start();

if (! isset($_SESSION['user'])) {
    header("Location: index.php");
}


?>