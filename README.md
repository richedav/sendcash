SQL

-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 04, 2015 at 04:52 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sendCash`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `locked` int(11) DEFAULT '0',
  `lastlogin` timestamp NULL DEFAULT NULL,
  `lastloginip` varchar(15) DEFAULT NULL,
  `validated-email` int(11) DEFAULT '0',
  `validated-mobile` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `created`, `locked`, `lastlogin`, `lastloginip`, `validated-email`, `validated-mobile`) VALUES
(1, 'david@candengo.com', '$2a$10$up0w2y1lYCmyKVSEhV9bqu83cZ.J7cwcGy/YlSXlViz5ROOCfnQGa', 'David', NULL, '2014-12-13 17:34:41', 0, NULL, NULL, 0, 0),
(4, 'david@unimpossible.co.uk', '$2a$10$gLCdxtJ3fYwISCuTbyFoLeqJosGfw3FW413AlNZDceVT8QmfZe.iu', 'david', NULL, '2014-12-13 17:36:44', 0, '2014-12-22 19:59:41', '192.168.0.204', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
